<?php

//Registering the composer includes.
include_once(__DIR__ . '/vendor/autoload.php');

//Registering all the classes and interfaces.
include_once(__DIR__ . '/lib/AutoLoader.php');
include_once(__DIR__ . '/lib/Container.php');

use OO_NFePHP\AutoLoader;

spl_autoload_register(array('OO_NFePHP\AutoLoader', 'loadClass'));
AutoLoader::registerDirectory(__DIR__ . '/src');
