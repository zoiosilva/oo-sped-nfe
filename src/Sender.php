<?php
declare(strict_types=1);

namespace OO_NFePHP;

use OO_NFePHP\Container;

use OO_NFePHP\Interfaces\IEstado;
use OO_NFePHP\Interfaces\IPrestador;

use NFePHP\Common\Certificate;
use NFePHP\NFe\Tools;

/**
 * Classe que irá comunicar-se com o webservice do estado configurado.
 */
final class Sender
{
    /**
     * Versão do leiaute da NF-e sendo usado.
     * @var string
     */
    private const VERSAO_NFE = '4.00';

    /**
     * Schema for the NF-e layout.
     * @since 1.0.0
     * @var string
     */
    private const SCHEME = 'PL_009_V4';

    /**
     * Objeto do componente de comunicação com os servidores dos estados.
     * @var NFePHP\NFe\Tools
     */
    private $tools;

    /**
     * @param int $tipoAmbiente (1-Ambiente de Produção, 2-Ambiente de homologação).
     * @param string $certificatePath Caminho físico para o certificado digital pfx.
     * @param string $certificatePassword Senha do certificado digital.
     */
    public function __construct(
        int $tipoAmbiente,
        string $certificatePath,
        string $certificatePassword
    ) {
        $config = $this->getConfigJSon($tipoAmbiente);

        $certificate = Container::instance()->call(Certificate::class, 'readPfx', array(
            'content' => file_get_contents($certificatePath),
            'password' => $certificatePassword,
        ));

        $this->tools = Container::instancia()->get(Tools::class, array(
            'configJson' => $config,
            'certificate' => $certificate,
        ));
    }

    /**
     * Envia a nota fiscal para o webservice do estado. Processo síncrono.
     * @param string $xmlDaNota xml da nota fiscal gerada.
     * @param string $idLote Identificação do lote de notas fiscais.
     * @return string A resposta do webservice.
     */
    public function enviarNotaSincrono(string $xmlDaNota, string $idLote): string
    {
        $lote = array(
            $xmlDaNota
        );
        $envioSincrono = 1;

        $response = $this->tools->sefazEnviaLote($lote, $idLote, $envioSincrono);

        return $response;
    }

    /**
     * Carrega json de configuração para passar para o componente de NF-e.
     *
     * @param int $tipoAmbiente (1-Ambiente de Produção, 2-Ambiente de homologação).
     * @return string
     */
    private function getConfigJSon(int $tipoAmbiente): string
    {
        $prestador = Container::instance()->get(IPrestador::class);
        $estado = Container::instance()->get('EnderecoPrestador');

        $config = array(
            'tpAmb' => $tipoAmbiente,
            'razaosocial' => $prestador->getNomePrestador(),
            'cnpj' => $prestador->getCNPJ(),
            'siglaUF' => $estado->getSiglaUF(),
            'schemes' => self::SCHEME,
            'versao' => self::VERSAO_NFE,
        );

        return json_encode($config);
    }
}
