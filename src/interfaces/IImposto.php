<?php
declare(strict_types=1);

namespace OO_NFePHP\Interfaces;

/**
 * Tributos incidentes nos produtos ou serviços da NF-e.
 */
interface IImposto
{
    /**
     * Valor estimado total de impostos federais, estaduais e municipais.
     * @return string
     */
    public function getValorTotalTributos(): string;
}
