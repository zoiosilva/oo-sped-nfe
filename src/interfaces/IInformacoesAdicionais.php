<?php
declare(strict_types=1);

namespace OO_NFePHP\Interfaces;

/**
 * Informações adicionais da NF-e.
 */
interface IInformacoesAdicionais
{
    /**
     * Informações adicionais de interesse do Fisco (v2.0).
     * @return string
     */
    public function getInformacoesAoFisco(): string;

    /**
     * Informações complementares de interesse do Contribuinte.
     * @return string
     */
    public function getInformacoesComplementares(): string;
}
