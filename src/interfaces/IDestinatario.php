<?php
declare(strict_types=1);

namespace OO_NFePHP\Interfaces;

/**
 * Dados da Identificação do destinatário.
 */
interface IDestinatario extends IEmail
{
    /**
     * Verdadeiro caso PF, falso caso PJ ou estrangeiro.
     * @return bool
     */
    public function ehPessoaFisica(): bool;

    /**
     * Número do CPF, caso pessoa física.
     * @return string
     */
    public function getCPF(): string;

    /**
     * Número do CNPJ, caso pessoa jurídica.
     * @return string
     */
    public function getCNPJ(): string;

    /**
     * Razão Social ou nome do destinatário;
     * @return string
     */
    public function getNome(): string;

    /**
     * Número da inscrição estadual, caso PJ.
     * @return string
     */
    public function getInscricaoEstadual(): string;

    /**
     * Indicador da IE do destinatário Pessoa Juídica:
     * 1. Contribuinte ICMS pagamento à vista;
     * 2. Contribuinte isento de inscrição;
     * 9. Não Contribuinte.
     * @return string
     */
    public function getIndicadorIePj(): string;
}
