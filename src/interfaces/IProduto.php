<?php
declare(strict_types=1);

namespace OO_NFePHP\Interfaces;

/**
 * Dados dos produtos e serviços da NF-e.
 */
interface IProduto
{
    /**
     * Código do produto ou serviço.
     * Preencher com CFOP caso se trate de itens não relacionados com mercadorias/produto
     * e que o contribuinte não possua codificação própria.
     * Formato ”CFOP9999”.
     *
     * maxLength value="60".
     * @return string
     */
    public function getCodigoProduto(): string;

    /**
     * GTIN (Global Trade Item Number) do produto, antigo código EAN ou código de barras.
     * @return string
     */
    public function getCodioEAN(): string;

    /**
     * Descrição do produto ou serviço.
     * @return string
     */
    public function getDescricaoProduto(): string;

    /**
     * Código NCM (8 posições), será permitida a informação do gênero (posição
     * do capítulo do NCM) quando a operação não for de comércio exterior
     * (importação/exportação) ou o produto não seja tributado pelo IPI. Em
     * caso de item de serviço ou item que não tenham produto (Ex.
     * transferência de crédito, crédito do ativo imobilizado, etc.), informar
     * o código 00 (zeros) (v2.0).
     * @link https://www.sefaz.mt.gov.br/portal/download/arquivos/Tabela_NCM.pdf
     * @return string
     */
    public function getCodigoNCM(): string;

    /**
     * Codigo especificador da Substuicao Tributaria - CEST, que identifica a
     * mercadoria sujeita aos regimes de  substituicao tributária e de
     * antecipação do recolhimento do imposto.
     * @return string
     */
    public function getCEST(): string;

    /**
     * Código Fiscal de Operações e Prestações - CFOP.
     * @link http://www.sefaz.rs.gov.br/ASP/SEF_root/INF/SEF-Sintegra-Info.htm#tabela
     * @return string
     */
    public function getCFOP(): string;

    /**
     * Unidade comercial do produto.
     * @return string
     */
    public function getUnidadeComercial(): string;

    /**
     * Quantidade Comercial do produto, alterado para aceitar de 0 a 4 casas decimais e 11 inteiros.
     * @return string
     */
    public function getQuantidadeComercial(): string;

    /**
     * Valor unitário de comercialização - alterado para aceitar 0 a 10 casas decimais e 11 inteiros.
     * @return string
     */
    public function getValorUniarioComercializacao(): string;

    /**
     * Valor bruto do produto ou serviço.
     * @return string
     */
    public function getValorBrutoProduto(): string;

    /**
     * GTIN (Global Trade Item Number) da unidade tributável, antigo código EAN ou código de barras
     * @return string
     */
    public function getEANUnidadeTributavel(): string;

    /**
     * Unidade tributável do produto.
     * @return string
     */
    public function getUnidadeTributavel(): string;

    /**
     * Quantidade Tributável - alterado para aceitar de 0 a 4 casas decimais e 11 inteiros.
     * @return string
     */
    public function getQuantidadeTributavel(): string;

    /**
     * Valor unitário de tributação - - alterado para aceitar 0 a 10 casas decimais e 11 inteiros.
     * @return string
     */
    public function getValorUnitarioTributacao(): string;

    /**
     * Valor Total do Frete.
     * @return string
     */
    public function getValorFrete(): string;

    /**
     * Valor do desconto.
     * @return string
     */
    public function getValorDesconto(): string;

    /**
     * Este campo deverá ser preenchido com:
     *  - 0 - o valor do item (vProd) não compõe o valor total da NF-e (vProd);
     *  - 1 - o valor do item (vProd) compõe o valor total da NF-e (vProd);
     * @return string
     */
    public function getIndicadorValorTotal(): string;

    /**
     * Pedido de compra - Informação de interesse do emissor para controle do B2B.
     * @return string
     */
    public function getPedidoDeCompra(): string;

    /**
     * Número do Item do Pedido de Compra - Identificação do número do item do pedido de Compra.
     * @return string
     */
    public function getNumeroItemPedidoDeCompra(): string;
}
