<?php
declare(strict_types=1);

namespace OO_NFePHP\Interfaces;

/**
 * Dados da Identificação do emitente.
 */
interface IPrestador
{
    /**
     * Número do CNPJ do emitente.
     * @return string
     */
    public function getCNPJ(): string;

    /**
     * Razão Social ou Nome do emitente.
     * @return string
     */
    public function getNomePrestador(): string;

    /**
     * Inscrição Estadual do Emitente.
     * @return string
     */
    public function getInscricaoEstadual(): string;

    /**
     * Inscrição Municipal.
     *
     * maxLength value="15".
     * @return string
     */
    public function getInscricaoMunicipal(): string;

    /**
     * Código do CNAE Fiscal.
     * @return string
     */
    public function getCNAE(): string;

    /**
     * Código de Regime Tributário.
     * Este campo será obrigatoriamente preenchido com:
     * - 1–Simples Nacional;
     * - 2–Simples Nacional – excesso de sublimite de receita bruta;
     * - 3–Regime Normal.
     * @return string
     */
    public function getCodigoRegimeTributario(): string;
}
