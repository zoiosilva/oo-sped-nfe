<?php
declare(strict_types=1);

namespace OO_NFePHP\Interfaces;

/**
 * Dados relacionados a identificação da nota fiscal.
 */
interface IIdentificacao extends IMunicipio, IEstado
{
    /**
     * Descrição da Natureza da Operação.
     * maxLength=60.
     * @return string
     */
    public function getNaturezaOperacao(): string;

    /**
     * Série do Documento Fiscal.
     * - série normal: 0-889.
     * - Avulsa Fisco: 890-899.
     * - SCAN: 900-999.
     *
     * pattern value="0|[1-9]{1}[0-9]{0,2}"
     * @return string
     */
    public function getSerieDoDocumentoFiscal(): string;

    /**
     * Número do Documento Fiscal.
     * pattern value="[1-9]{1}[0-9]{0,8}".
     * @return string
     */
    public function getNumeroNF(): string;

    /**
     * Versão do aplicativo utilizado no processo de emissão.
     *
     * maxLength value="20".
     * @return string
     */
    public function getVersaoCliente(): string;
}
