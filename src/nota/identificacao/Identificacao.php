<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Identificacao;

use OO_NFePHP\Interfaces\IIdentificacao;
use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * identificação da NF-e.
 */
final class Identificacao extends Makeable
{
    /**
     * Código do modelo do Documento Fiscal.
     * - 55 = NF-e;
     * - 65 = NFC-e.
     * @var string
     */
    private const MOD = '55';

    /**
     * Tipo do Documento Fiscal.
     * - 0 - entrada;
     * - 1 - saída.
     * @var string
     */
    private const TIPO_DOC = '1';

    /**
     * Identificador de Local de destino da operação
     * - 1-Interna;
     * - 2-Interestadual;
     * - 3-Exterior.
     * @var string
     */
    private const ID_DEST = '1';

    /**
     * Formato de impressão do DANFE
     * - 0-sem DANFE;
     * - 1-DANFe Retrato;
     * - 2-DANFe Paisagem;
     * - 3-DANFe Simplificado;
     * - 4-DANFe NFC-e;
     * - 5-DANFe NFC-e em mensagem eletrônica.
     * @var string
     */
    private const TIPO_IMPRESSAO = '1';

    /**
     * Forma de emissão da NF-e
     * - 1 - Normal;
     * - 2 - Contingência FS;
     * - 3 - Contingência SCAN;
     * - 4 - Contingência DPEC;
     * - 5 - Contingência FSDA;
     * - 6 - Contingência SVC - AN;
     * - 7 - Contingência SVC - RS;
     * - 9 - Contingência off-line NFC-e.
     * @var string
     */
    private const TIPO_EMISSAO = '1';

    /**
     * Finalidade da emissão da NF-e:
     * - 1 - NFe normal;
     * - 2 - NFe complementar;
     * - 3 - NFe de ajuste;
     * - 4 - Devolução/Retorno.
     * @var string
     */
    private const FINALIDADE_NFE = '1';

    /**
     * Indica operação com consumidor final
     * - 0-Não;
     * - 1-Consumidor Final.
     * @var string
     */
    private const CONSUMIDOR_FINAL = '1';

    /**
     * Indicador de presença do comprador no estabelecimento comercial no momento da oepração:
     * - 0-Não se aplica (ex.: Nota Fiscal complementar ou de ajuste);
     * - 1-Operação presencial;
     * - 2-Não presencial, internet;
     * - 3-Não presencial, teleatendimento;
     * - 4-NFC-e entrega em domicílio;
     * - 5-Operação presencial, fora do estabelecimento;
     * - 9-Não presencial, outros.
     * @var string
     */
    private const PRESENCIAL = '2';

    /**
     * Processo de emissão utilizado com a seguinte codificação:
     * - 0 - emissão de NF-e com aplicativo do contribuinte;
     * - 1 - emissão de NF-e avulsa pelo Fisco;
     * - 2 - emissão de NF-e avulsa, pelo contribuinte com seu certificado digital, através do site do Fisco;
     * - 3 - emissão de NF-e pelo contribuinte com aplicativo fornecido pelo Fisco.
     * @var string
     */
    private const PROCESSO_EMISSAO = '0';
    
    private const CODIGO_NF_MIN = 0;
    private const CODIGO_NF_MAX = 99999999;
    private const CODIGO_NF_LEN = 8;
    private const CODIGO_NF_PAD = '0';

    private const DIGITO_MIN = 0;
    private const DIGITO_MAX = 9;

    /**
     * Identificação do Ambiente:
     * - 1-Produção;
     * - 2-Homologação
     * @var int
     */
    private $ambiente;

    /**
     * Dados fornecidos pela aplicação em uso.
     * @var IIdentificacao
     */
    private $dados;

    /**
     * @param IIdentificacao $dados Informações para serem preenchidas no elemento.
     * @param int $ambiente 1-Ambiente de produção; 2-Ambiente de homologação.
     */
    public function __construct(IIdentificacao $dados, int $ambiente = 2)
    {
        parent::__construct('ide');
        $this->dados = $dados;
        $this->ambiente = $ambiente;
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->cUF = $this->dados->getCodigoUF();
        $s->cNF = $this->getCodigoNF();
        $s->natOp = $this->dados->getNaturezaOperacao();
        $s->mod = self::MOD;
        $s->serie = $this->dados->getSerieDoDocumentoFiscal();
        $s->nNF = $this->dados->getNumeroNF();
        $s->dhEmi = date(DATE_W3C);
        // $s->dhSaiEnt;
        $s->tpNF = self::TIPO_DOC;
        $s->idDest = self::ID_DEST;
        $s->cMunFG = $this->dados->getCodigoCidade();
        $s->tpImp = self::TIPO_IMPRESSAO;
        $s->tpEmis = self::TIPO_EMISSAO;
        $s->cDV = $this->getDigitoVerificador();
        $s->tpAmb = $this->ambiente;
        $s->finNFe = self::FINALIDADE_NFE;
        $s->indFinal = self::CONSUMIDOR_FINAL;
        $s->indPres = self::PRESENCIAL;
        $s->procEmi = self::PROCESSO_EMISSAO;
        $s->verProc = $this->dados->getVersaoCliente();
        // if ($contingencia) {
        //     $s->dhCont;
        //     $s->xJust;
        // }

        return $s;
    }
    
    /**
     * Código numérico que compõe a Chave de Acesso.
     * Número aleatório gerado pelo emitente para cada NF-e.
     *
     * pattern value="[0-9]{8}".
     * @return string
     */
    private function getCodigoNF(): string
    {
        $random = strval(rand(self::CODIGO_NF_MIN, self::CODIGO_NF_MAX));
        return str_pad($random, self::CODIGO_NF_LEN, self::CODIGO_NF_PAD, STR_PAD_LEFT);
    }

    /**
     * Digito Verificador da Chave de Acesso da NF-e.
     *
     * pattern value="[0-9]{1}"
     * @return string
     */
    private function getDigitoVerificador(): string
    {
        return strval(rand(self::DIGITO_MIN, self::DIGITO_MAX));
    }
}
