<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Informacoes;

use OO_NFePHP\Interfaces\IInformacoesAdicionais;
use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Informações adicionais da NF-e.
 */
final class InformacoesAdicionais extends Makeable
{
    /**
     * Dados com as informações adicionais.
     * @var IInformacoesAdicionais
     */
    private $informacoes;

    /**
     * @param IInformacoesAdicionais $informacoes Dados com as informações adicionais.
     */
    public function __construct(IInformacoesAdicionais $informacoes)
    {
        parent::__construct('infAdic');
        $this->informacoes = $informacoes;
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->infAdFisco = $this->informacoes->getInformacoesAoFisco();
        $s->infCpl = $this->informacoes->getInformacoesComplementares();

        return $s;
    }
}
