<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Informacoes;

use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Node principal. Informações da Nota Fiscal eletrônica.
 */
final class Informacoes extends Makeable
{
    private const PK_NITEM = null;
    private const VERSAO = '4.00';
    private $id;

    /**
     * @param string|null $id Se o Id de 44 digitos não for passado, será gerado automaticamente.
     */
    public function __construct(string $id = null)
    {
        parent::__construct('infNFe');
        $this->id = $id;
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->versao = self::VERSAO;
        $s->Id = $this->id;
        $s->pk_nItem = self::PK_NITEM;

        return $s;
    }
}
