<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Detalhes;

use OO_NFePHP\Interfaces\IProduto;
use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Dados dos produtos e serviços da NF-e.
 */
final class Produto extends Makeable
{
    /**
     * Índice do Item da NF-e.
     * @var string
     */
    private $item;

    /**
     * Classe com os dados do produto.
     * @var IProduto
     */
    private $produto;

    /**
     * @param string $item Índice do item da NF-e.
     * @param IProduto $produto Classe com os dados do produto.
     */
    public function __construct(string $item, IProduto $produto)
    {
        parent::__construct('prod');
        $this->item = $item;
        $this->produto = $produto;
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->item = $this->item;
        $s->cProd = $this->produto->getCodigoProduto();
        $s->cEAN = $this->produto->getCodioEAN();
        $s->xProd = $this->produto->getDescricaoProduto();
        $s->NCM = $this->produto->getCodigoNCM();
        $CEST = $this->produto->getCEST();
        if (!empty($CEST)) {
            $s->CEST = $CEST;
        }
        $s->CFOP = $this->produto->getCFOP();
        $s->uCom = $this->produto->getUnidadeComercial();
        $s->qCom = $this->produto->getQuantidadeComercial();
        $s->vUnCom = $this->produto->getValorUniarioComercializacao();
        $s->vProd = $this->produto->getValorBrutoProduto();
        $s->cEANTrib = $this->produto->getEANUnidadeTributavel();
        $s->uTrib = $this->produto->getUnidadeTributavel();
        $s->qTrib = $this->produto->getQuantidadeTributavel();
        $s->vUnTrib = $this->produto->getValorUnitarioTributacao();
        $vDesc = $this->produto->getValorDesconto();
        if (!empty($vDesc)) {
            $s->vDesc = $vDesc;
        }
        $s->indTot = $this->produto->getIndicadorValorTotal();
        $xPed = $this->produto->getPedidoDeCompra();
        if (!empty($xPed)) {
            $s->xPed = $xPed;
        }

        return $s;
    }
}
