<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Detalhes;

use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Dados do ICMS Normal e ST.
 * Tributação do ICMS pelo SIMPLES NACIONAL e CSOSN=102, 103, 300 ou 400 (v.2.0))
 */
final class IcmsSn extends Makeable
{
    /**
     * Índice do item da NF-e.
     * @var string
     */
    private $item;

    /**
     * Origem da mercadoria:
     * - 0 - Nacional;
     * - 1 - Estrangeira - Importação direta;
     * - 2 - Estrangeira - Adquirida no mercado interno (v2.0).,
     * @var string
     */
    private const ORIGEM_MERCADORIA = '0';

    /**
     * Código CSON considerado na classe.
     * @var string
     */
    private const CSON = '102';

    /**
     * @param string $item Índice do item da NF-e.
     */
    public function __construct(string $item)
    {
        parent::__construct('ICMSSN');
        $this->item = $item;
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->item = $this->item;
        $s->orig = self::ORIGEM_MERCADORIA;
        $s->CSOSN = self::CSON;

        return $s;
    }
}
