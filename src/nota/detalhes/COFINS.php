<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Detalhes;

use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Dados do COFINS.
 */
final class COFINS extends Makeable
{
    /**
     * Código de Situação Tributária do COFINS.
     * - 01 – Operação Tributável - Base de Cálculo = Valor da Operação Alíquota Normal (Cumulativo/Não Cumulativo);
     * - 02 – Operação Tributável - Base de Calculo = Valor da Operação (Alíquota Diferenciada);
     * @var string
     */
    private const CST = '04';

    /**
     * Índice do item da NF-e.
     * @var string
     */
    private $item;
  
    public function __construct(string $item)
    {
        parent::__construct('COFINS');
        $this->item = $item;
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->item = $this->item;
        $s->CST = self::CST;
        // $s->vBC = null;
        // $s->pCOFINS = null;
        // $s->vCOFINS = null;
        // $s->qBCProd = null;
        // $s->vAliqProd = null;

        return $s;
    }
}
