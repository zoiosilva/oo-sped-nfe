<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Transporte;

use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Dados dos transportes da NF-e.
 */
final class Transporte extends Makeable
{
    /**
     * Modalidade do frete:
     * 0- Contratação do Frete por conta do Remetente (CIF);
     * 1- Contratação do Frete por conta do destinatário/remetente (FOB);
     * 2- Contratação do Frete por conta de terceiros;
     * 3- Transporte próprio por conta do remetente;
     * 4- Transporte próprio por conta do destinatário;
     * 9- Sem Ocorrência de transporte.
     * @var string
     */
    private const MODALIDADE_FRETE = '0';

    public function __construct()
    {
        parent::__construct('transp');
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->modFrete = self::MODALIDADE_FRETE;

        return $s;
    }
}
