<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Destinatario;

use OO_NFePHP\Interfaces\IDestinatario;
use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Identificação do Destinatário.
 */
final class Destinatario extends Makeable
{
    /**
     * Indicador da IE do destinatário:
     * - 1–Contribuinte ICMSpagamento à vista;
     * - 2–Contribuinte isento de inscrição;
     * - 9–Não Contribuinte.
     * @var string
     */
    private const INDICADOR_IE_PF = '2';

    /**
     * Dados do destinatário.
     * @var IDestinatario
     */
    private $destinatario;

    /**
     * @param IDestinatario $destinatario Os dados do destinatário para ser considerado no documento.
     */
    public function __construct(IDestinatario $destinatario)
    {
        parent::__construct('dest');
        $this->destinatario = $destinatario;
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();

        if ($this->destinatario->ehPessoaFisica()) {
            $this->buildPessoaFisica($s);
        } else {
            $this->buildPessoaJuridica($s);
        }

        return $s;
    }

    /**
     * Preenche as informações relacionadas a pessoa física.
     * @param stdClass $s Classe que representa o elemento xml.
     * @return void
     */
    private function buildPessoaFisica(stdClass &$s): void
    {
        $s->CPF = $this->destinatario->getCPF();
        $s->xNome = $this->destinatario->getNome();
        $s->indIEDest = self::INDICADOR_IE_PF;
        $s->email = $this->destinatario->getEmailPrincipal();
    }

    /**
     * Preenche as informações relacionadas a pessoa jurídica.
     * @param stdClass $s Classe que representa o elemento xml.
     * @return void
     */
    private function buildPessoaJuridica(stdClass &$s): void
    {
        $s->CNPJ = $this->destinatario->getCNPJ();
        $s->xNome = $this->destinatario->getNome();
        $s->indIEDest = $this->destinatario->getIndicadorIePj();
        $s->IE = $this->destinatario->getInscricaoEstadual();
        $s->email = $this->destinatario->getEmailPrincipal();
    }
}
