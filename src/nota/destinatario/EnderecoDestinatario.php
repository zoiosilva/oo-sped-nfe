<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Destinatario;

use OO_NFePHP\Interfaces\IEndereco;
use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Dados do Endereço do Destinatário.
 */
final class EnderecoDestinatario extends Makeable
{
    /**
     * Classe com os dados do endereço do destinatário.
     * @var IEndereco
     */
    private $endereco;

    /**
     * @param IEndereco $endereco Dados do endereço do destinatário.
     */
    public function __construct(IEndereco $endereco)
    {
        parent::__construct('enderDest');
        $this->endereco = $endereco;
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->xLgr = $this->endereco->getLogradouro();
        $s->nro = $this->endereco->getNumeroLogradouro();
        $xCpl = $this->endereco->getComplementoEndereco();
        if (!empty($xCpl)) {
            $s->xCpl = $xCpl;
        }
        $s->xBairro = $this->endereco->getBairro();
        $s->cMun = $this->endereco->getCodigoCidade();
        $s->xMun = $this->endereco->getNomeCidade();
        $s->UF = $this->endereco->getSiglaUF();
        $s->CEP = $this->endereco->getCEP();
        $cPais = $this->endereco->getCodigoPais();
        if (!empty($cPais)) {
            $s->cPais = $cPais;
        }
        $xPais = $this->endereco->getNomePais();
        if (!empty($xPais)) {
            $s->xPais = $xPais;
        }
        $fone = $this->endereco->getNumeroTelefonePrincipal();
        if (!empty($fone)) {
            $s->fone = $fone;
        }

        return $s;
    }
}
