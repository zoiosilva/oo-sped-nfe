<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Total;

use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Totais referentes ao ICMS.
 *
 * NOTA: Esta tag não necessita que sejam passados valores,
 * pois a classe irá calcular esses totais e irá usar essa
 * totalização para complementar e gerar esse node, caso
 * nenhum valor seja passado como parâmetro.
 */
final class ICMSTotal extends Makeable
{
    public function __construct()
    {
        parent::__construct('ICMSTot');
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->vBC = null;
        $s->vICMS = null;
        $s->vICMSDeson = null;
        $s->vFCPUFDest = null;
        $s->vICMSUFDest = null;
        $s->vICMSUFRemet = null;
        $s->vFCP = null;
        $s->vBCST = null;
        $s->vST = null;
        $s->vFCPST = null;
        $s->vFCPSTRet = null;
        $s->vProd = null;
        $s->vFrete = null;
        $s->vSeg = null;
        $s->vDesc = null;
        $s->vII = null;
        $s->vIPI = null;
        $s->vIPIDevol = null;
        $s->vPIS = null;
        $s->vCOFINS = null;
        $s->vOutro = null;
        $s->vNF = null;
        $s->vTotTrib = null;

        return $s;
    }
}
