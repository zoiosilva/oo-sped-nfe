<?php
declare(strict_types=1);

namespace OO_NFePHP;

use OO_NFePHP\Container;
use OO_NFePHP\Signer;

use OO_NFePHP\Interfaces\IDestinatario;
use OO_NFePHP\Interfaces\IDetalhesPagamento;
use OO_NFePHP\Interfaces\IEndereco;
use OO_NFePHP\Interfaces\IIdentificacao;
use OO_NFePHP\Interfaces\IInformacoesAdicionais;
use OO_NFePHP\Interfaces\IPrestador;
use OO_NFePHP\Interfaces\ITransportadora;
use OO_NFePHP\Interfaces\IVolume;

use OO_NFePHP\Nota\Makeable;
use OO_NFePHP\Nota\Destinatario\Destinatario;
use OO_NFePHP\Nota\Destinatario\EnderecoDestinatario;
use OO_NFePHP\Nota\Detalhes\COFINS;
use OO_NFePHP\Nota\Detalhes\IcmsSn;
use OO_NFePHP\Nota\Detalhes\Imposto;
use OO_NFePHP\Nota\Detalhes\PIS;
use OO_NFePHP\Nota\Detalhes\Produto;
use OO_NFePHP\Nota\Emitente\EnderecoPrestador;
use OO_NFePHP\Nota\Emitente\Prestador;
use OO_NFePHP\Nota\Identificacao\Identificacao;
use OO_NFePHP\Nota\Informacoes\Informacoes;
use OO_NFePHP\Nota\Informacoes\InformacoesAdicionais;
use OO_NFePHP\Nota\Informacoes\InformacoesContribuinte;
use OO_NFePHP\Nota\Pagamento\DetalhesPagamento;
use OO_NFePHP\Nota\Pagamento\Pagamento;
use OO_NFePHP\Nota\Total\ICMSTotal;
use OO_NFePHP\Nota\Transporte\Transportadora;
use OO_NFePHP\Nota\Transporte\Transporte;
use OO_NFePHP\Nota\Transporte\Volume;

use NFePHP\NFe\Make;

/**
 * Classe que irá fazer a consutrução da nota fiscal,
 * distribuindo todas as informações fornecidas nos
 * locais corretos.
 */
final class Builder
{
    /**
     * Aqui estão as instâncias das classes que irão compor os elementos da nota.
     * @var Makeable[]
     */
    private $elementos;

    /**
     * Aqui fica armazenado o xml da nota resultado da chamada da função build,
     * e da função sign.
     * @var string
     */
    private $xml = '';

    /**
     * Mensagens de erro ou problemas serão acumuladas aqui.
     * @var string
     */
    private $message;

    /**
     * @param int $ambiente (1-produção; 2-homologação).
     * @param IDestinatario $destinatario Dados do destinatário (comprador) da nota.
     * @param array $detalhes Um array de `IProduto` e `IImposto`, com os itens comercializados.
     * @param IInformacoesContribuinte[] $informacoesAoContribuinte Array com informações adicionais ao contribuinte.
     */
    public function __construct(
        int $ambiente,
        IDestinatario $destinatario,
        array $detalhes,
        array $informacoesAoContribuinte = array()
    ) {
        /**
         * The dependency injector.
         * @var Container
         */
        $di = Container::instance();

        $this->elementos = array(
            //Raiz da nota
            $di->get(Informacoes::class),

            //Identificação da nota
            $di->get(Identificacao::class, array(
                'ambiente' => $ambiente
            )),

            //Emitente
            $di->get(Prestador::class),
            $di->get(EnderecoPrestador::class, array(
                'endereco' => $di->get('EnderecoPrestador'),
            )),

            //Destinatário
            $di->get(Destinatario::class, array(
                'destinatario' => $destinatario
            )),
            $di->get(EnderecoDestinatario::class, array(
                'endereco' => $di->get('EnderecoDestinatario'),
            )),
        );

        //detalhes
        foreach ($detalhes as $indice => $detalhe) {
            $item = strval($indice + 1);
            $produto = $detalhe[0];
            $imposto = $detalhe[1];

            $this->elementos[] = $di->get(Produto::class, array(
                'item' => $item,
                'produto' => $produto,
            ));
            $this->elementos[] = $di->get(Imposto::class, array(
                'item' => $item,
                'imposto' => $imposto
            ));
            $this->elementos[] = $di->get(IcmsSn::class, array('item' => $item));
            $this->elementos[] = $di->get(PIS::class, array('item' => $item));
            $this->elementos[] = $di->get(COFINS::class, array('item' => $item));
        }

        //Total
        $this->elementos[] = $di->get(ICMSTotal::class);

        //Transporte
        $this->elementos[] = $di->get(Transporte::class);
        $this->elementos[] = $di->get(Transportadora::class);
        $this->elementos[] = $di->get(Volume::class);

        //Pagamento
        $this->elementos[] = $di->get(Pagamento::class);
        $this->elementos[] = $di->get(DetalhesPagamento::class);

        //Informações Adicionais
        if ($di->has(InformacoesAdicionais::class)) {
            $this->elementos[] = $di->get(InformacoesAdicionais::class);
        }

        foreach ($informacoesAoContribuinte as $informacao) {
            $this->elementos[] = $di->get(InformacoesContribuinte::class, array(
                'informacoes' => $informacao
            ));
        }

        $this->message = '';
    }

    /**
     * Constrói o documento que representa a nota fiscal.
     * @return bool Verdadeiro, se deu certo. Falso, caso ocorra erro.
     */
    public function build(): bool
    {
        $make = Container::instance()->get(Make::class);

        foreach ($this->elementos as $elemento) {
            $elemento->make($make);
        }

        if ($make->monta()) {
            $this->xml = $make->getXML();
            return true;
        }

        $this->xml = '';
        return false;
    }

    /**
     * Assina o arquivo compilado.
     * @param string $certificatePath Caminho completo para o certificado digital pfx.
     * @param string $certificatePassword Senha do certificado digital.
     * @return bool Falso caso o arquivo não possa ser assinado.
     */
    public function sign(string $certificatePath, string $certificatePassword): bool
    {
        /**
         * @var Signer
         */
        $signer = Container::instance()->get(Signer::class, array(
            'certificatePath' => $certificatePath,
            'certificatePassword' => $certificatePassword,
        ));

        if (!$signer->sign($this->xml)) {
            $this->message = $signer->getMessage();
            return false;
        }
        
        return true;
    }
    
    /**
     * Obtém o texto, string, que representa o xml da nota.
     * @return string
     */
    public function getXML(): string
    {
        return $this->xml;
    }

    /**
     * Obtém mensagens de ocorrências de erros na geração do XML da nota fiscal.
     * @return string
     */
    public function getErrorMessage(): string
    {
        return $this->message;
    }
}
