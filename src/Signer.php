<?php
declare(strict_types=1);

namespace OO_NFePHP;

use OO_NFePHP\Container;
use OO_NFePHP\Interfaces\IMensagem;

use OO_NFePHP\Signer\ValidationItem;
use OO_NFePHP\Signer\ArquivoVazio;
use OO_NFePHP\Signer\CaminhoInvalido;
use OO_NFePHP\Signer\Loader;

use NFePHP\Common\Signer as Sgn;
use NFePHP\Common\Certificate;

/**
 * Used to sign a xml file.
 */
final class Signer
{
    /**
     * The path to the pfx file.
     * @var string
     */
    private $certificatePath;

    /**
     * The password for the pfx file.
     * @var string
     */
    private $certificatePassword;

    /**
     * The certificate instance.
     * @var Certificate
     */
    private $certificate;

    /**
     * Error messages.
     * @var string
     */
    private $message = '';

    /**
     * @var IMensagem
     */
    private $msg;

    /**
     * @var ValidationItem[]
     */
    private $validationChain;

    /**
     * @param string $certificatePath The pfx file.
     * @param string $certificatePassword The password to the file.
     */
    public function __construct(string $certificatePath, string $certificatePassword)
    {
        $this->certificatePath = $certificatePath;
        $this->certificatePassword = $certificatePassword;
        $this->msg = Container::instance()->get(IMensagem::class);
        $this->loadValidationChain();
    }

    /**
     * Load the certificate validation classes.
     * @return void
     */
    private function loadValidationChain(): void
    {
        $this->validationChain = array(
            Container::instance()->get(CaminhoInvalido::class, array(
                'certificatePath' => $this->certificatePath,
            )),
            Container::instance()->get(ArquivoVazio::class),
        );
    }

    /**
     * Sign the xml file.
     * @param string $xml The content to be signed.
     * @return bool
     */
    public function sign(string &$xml): bool
    {
        return $this->isValid($xml)
            && $this->tryLoadCertificate()
            && $this->trySign($xml);
    }

    /**
     * Try to sign the xml content.
     * @param string $xml the content to be signed.
     * @return bool
     */
    private function trySign(string &$xml): bool
    {
        try {
            $xml = Container::instance()->call(Sgn::class, 'sign', array(
                'certificate' => $this->certificate,
                'content' => $xml,
                'tagname' => 'infNFe',
            ));
        } catch (Exception $ex) {
            $this->message = $this->msg->naoFoiPossivelAssinarOArquivoXML($ex->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Tries to load the certificate.
     * @return bool
     */
    private function tryLoadCertificate(): bool
    {
        try {
            $this->certificate = Container::instance()->call(Certificate::class, 'readPfx', array(
                'content' => file_get_contents($this->certificatePath),
                'password' => $this->certificatePassword,
            ));
        } catch (Exception $ex) {
            $this->message = $this->msg->erroNaLeituraDoCertificadoDigital($ex->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Gets the current error message. If no errors ocurred, it will return an empty string.
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * Checks if all is good to sign the xml content.
     * @param string $xml the xml content that will be validated.
     * @return bool
     */
    private function isValid(string $xml): bool
    {
        foreach ($this->validationChain as $validationItem) {
            if (!$validationItem->validate($xml, $this->message)) {
                return false;
            }
        }
        return true;
    }
}
