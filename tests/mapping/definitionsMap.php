<?php

use OO_NFePHP\Interfaces\IDestinatario;
use OO_NFePHP\Interfaces\IDetalhesPagamento;
use OO_NFePHP\Interfaces\IEndereco;
use OO_NFePHP\Interfaces\IIdentificacao;
use OO_NFePHP\Interfaces\IInformacoesAdicionais;
use OO_NFePHP\Interfaces\IInformacoesContribuinte;
use OO_NFePHP\Interfaces\IMensagem;
use OO_NFePHP\Interfaces\IPrestador;
use OO_NFePHP\Interfaces\ITransportadora;
use OO_NFePHP\Interfaces\IVolume;
use OO_NFePHP\Interfaces\IProduto;
use OO_NFePHP\Interfaces\IImposto;

use OO_NFePHPTest\Mocks\DestinatarioMock;
use OO_NFePHPTest\Mocks\DestinatarioPJMock;
use OO_NFePHPTest\Mocks\DetalhesPagamentoMock;
use OO_NFePHPTest\Mocks\EnderecoPrestadorMock;
use OO_NFePHPTest\Mocks\EnderecoDestinatarioMock;
use OO_NFePHPTest\Mocks\IdentificacaoMock;
use OO_NFePHPTest\Mocks\InformacoesAdicionaisMock;
use OO_NFePHPTest\Mocks\MensagemMock;
use OO_NFePHPTest\Mocks\PrestadorMock;
use OO_NFePHPTest\Mocks\TransportadoraMock;
use OO_NFePHPTest\Mocks\VolumeMock;
use OO_NFePHPTest\Mocks\ProdutoMock;
use OO_NFePHPTest\Mocks\ImpostoMock;
use OO_NFePHPTest\Mocks\InformacoesContribuinteMock;

use function DI\create;

return array(
    'DestinatarioPF' => create(DestinatarioMock::class),
    'DestinatarioPJ' => create(DestinatarioPJMock::class),
    IDetalhesPagamento::class => create(DetalhesPagamentoMock::class),
    'EnderecoPrestador' => create(EnderecoPrestadorMock::class),
    'EnderecoDestinatario' => create(EnderecoDestinatarioMock::class),
    IIdentificacao::class => create(IdentificacaoMock::class),
    IInformacoesAdicionais::class => create(InformacoesAdicionaisMock::class),
    IMensagem::class => create(MensagemMock::class),
    IPrestador::class => create(PrestadorMock::class),
    ITransportadora::class => create(TransportadoraMock::class),
    IVolume::class => create(VolumeMock::class),
    IProduto::class => create(ProdutoMock::class),
    IImposto::class => create(ImpostoMock::class),
    IInformacoesContribuinte::class => create(InformacoesContribuinteMock::class),
);
