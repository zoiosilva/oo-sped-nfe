<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Nota\Transporte\Transportadora;

use PHPUnit\Framework\TestCase;
use OO_NFePHP\Nota\Transporte\Transportadora;
use OO_NFePHPTest\Mocks\TransportadoraMock;
use OO_NFePHPTest\Mocks\MakeMock;
use \stdClass;

final class BuildFieldsTest extends TestCase
{
    public function testDeveConstruirElementoCorretamenteQuandoNaoHouverLogradouro(): void
    {
        $this->chamarClasse(true);
    }

    public function testDeveConstruirElementoCorretamenteQuandoHouverLogradouro(): void
    {
        $this->chamarClasse(false);
    }

    private function chamarClasse(bool $logradouroVazio): void
    {
        $assertion = function (stdClass $arg) use ($logradouroVazio) {
            $this->assertObjectHasAttribute('xEnder', $arg);
            $logradouroVazio
                ? $this->assertEmpty($arg->xEnder)
                : $this->assertNotEmpty($arg->xEnder);
        };
        
        $make = new MakeMock($assertion);
        
        $dados = new TransportadoraMock($logradouroVazio);
        $subject = new Transportadora($dados);
        $subject->make($make);
    }
}
