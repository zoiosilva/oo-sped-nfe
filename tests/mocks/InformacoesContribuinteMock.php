<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Mocks;

use OO_NFePHP\Interfaces\IInformacoesContribuinte;

final class InformacoesContribuinteMock implements IInformacoesContribuinte
{
    public function getNomeCampo(): string
    {
        return 'Codigo';
    }

    public function getTexto(): string
    {
        return '123456';
    }
}
