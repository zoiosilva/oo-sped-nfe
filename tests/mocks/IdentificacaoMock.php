<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Mocks;

use OO_NFePHP\Interfaces\IIdentificacao;

final class IdentificacaoMock implements IIdentificacao
{
    public function getNaturezaOperacao(): string
    {
        return 'Venda de testes de unidade.';
    }

    public function getSerieDoDocumentoFiscal(): string
    {
        return '0';
    }

    public function getNumeroNF(): string
    {
        return '1';
    }

    public function getVersaoCliente(): string
    {
        return '1.0.0';
    }

    public function getCodigoCidade(): string
    {
        return '1100015';
    }
  
    public function getNomeCidade(): string
    {
        return 'ALTA FLORESTA D\'OESTE';
    }

    public function getCodigoUF(): string
    {
        return '11';
    }

    public function getSiglaUF(): string
    {
        return 'RO';
    }
}
