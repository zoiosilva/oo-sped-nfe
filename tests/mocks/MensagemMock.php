<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Mocks;

use OO_NFePHP\Interfaces\IMensagem;

final class MensagemMock implements IMensagem
{
    public function __construct()
    {
    }

    public function caminhoDoCertificadoInvalidoOuArquivoInexistente(): string
    {
        return '';
    }

    public function ehNecessarioCompilarOXmlAntesDeAssinalo(): string
    {
        return '';
    }

    public function erroNaLeituraDoCertificadoDigital(string $errorMessage): string
    {
        return '';
    }

    public function naoFoiPossivelAssinarOArquivoXML(string $errorMessage): string
    {
        return '';
    }
}
