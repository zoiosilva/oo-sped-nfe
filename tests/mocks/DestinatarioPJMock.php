<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Mocks;

use OO_NFePHP\Interfaces\IDestinatario;

final class DestinatarioPJMock implements IDestinatario
{
    public function ehPessoaFisica(): bool
    {
        return false;
    }

    public function getCPF(): string
    {
        return '00000000000';
    }

    public function getCNPJ(): string
    {
        return '000000000000';
    }

    public function getNome(): string
    {
        return 'Testes de Unidade';
    }

    public function getInscricaoEstadual(): string
    {
        return '1234567';
    }

    public function getIndicadorIePj(): string
    {
        return '9';
    }

    public function getEmailPrincipal(): string
    {
        return 'teste@teste.com';
    }
}
