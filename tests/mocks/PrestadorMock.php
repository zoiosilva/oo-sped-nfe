<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Mocks;

use OO_NFePHP\Interfaces\IPrestador;

final class PrestadorMock implements IPrestador
{
    public function getCNPJ(): string
    {
        return '123123000110';
    }

    public function getNomePrestador(): string
    {
        return 'Prestador dos testes de unidade.';
    }

    public function getInscricaoEstadual(): string
    {
        return '9873214';
    }

    public function getInscricaoMunicipal(): string
    {
        return '9876543';
    }

    public function getCNAE(): string
    {
        return '1234567';
    }

    public function getCodigoRegimeTributario(): string
    {
        return '1';
    }
}
