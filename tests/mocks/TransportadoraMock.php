<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Mocks;

use OO_NFePHP\Interfaces\ITransportadora;

final class TransportadoraMock implements ITransportadora
{
    /**
     * Define se é para construir o objeto sem o nome da rua.
     * @var bool
     */
    private $ehlogradouroVazio;

    public function __construct(bool $ehlogradouroVazio = false)
    {
        $this->ehlogradouroVazio = $ehlogradouroVazio;
    }

    public function getCNPJ(): string
    {
        return '123123000199';
    }
  
    public function getNome(): string
    {
        return 'Correios';
    }

    public function getInscricaoEstadual(): string
    {
        return '9876543';
    }

    public function getLogradouro(): string
    {
        return $this->ehlogradouroVazio ? '' : 'Rua dos Correios';
    }

    public function getNumeroLogradouro(): string
    {
        return $this->ehlogradouroVazio ? '' : '55';
    }
    
    public function getCodigoCidade(): string
    {
        return '1100049';
    }
  
    public function getNomeCidade(): string
    {
        return 'CACOAL';
    }
    
    public function getCodigoUF(): string
    {
        return '11';
    }

    public function getSiglaUF(): string
    {
        return 'RO';
    }
}
