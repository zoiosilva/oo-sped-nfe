<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Mocks;

use OO_NFePHP\Interfaces\IImposto;

final class ImpostoMock implements IImposto
{
    public function getValorTotalTributos(): string
    {
        return '0';
    }
}
