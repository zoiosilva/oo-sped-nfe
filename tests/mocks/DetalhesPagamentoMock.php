<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Mocks;

use OO_NFePHP\Interfaces\IDetalhesPagamento;

final class DetalhesPagamentoMock implements IDetalhesPagamento
{
    public function getCondicaoPagamento(): string
    {
        return '0';
    }

    public function getFormaPagamento(): string
    {
        return '03';
    }

    public function getValorPagamento(): string
    {
        return '49.90';
    }

    public function getCNPJCartao(): string
    {
        return '123123000199';
    }

    public function getBandeiraCartao(): string
    {
        return '01';
    }

    public function getCodigoAutorizacao(): string
    {
        return '123456798';
    }
}
