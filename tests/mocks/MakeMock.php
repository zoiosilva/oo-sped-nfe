<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Mocks;

use NFePHP\NFe\Make;
use \stdClass;

/**
 * A mock class to the NF-e xml file maker.
 */
final class MakeMock extends Make
{
    /**
     * @var callable
     */
    private $f;

    /**
     * @param callable $func Some function to be called.
     */
    public function __construct(callable $func)
    {
        $this->f = $func;
    }
    
    public function tagtransporta(stdClass $arg0): void
    {
        $func = $this->f;
        $func($arg0);
    }
}
