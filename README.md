[![Latest Stable Version](https://poser.pugx.org/zoiosilva/oo-sped-nfe/v/stable)](https://packagist.org/packages/zoiosilva/oo-sped-nfe)
[![Total Downloads](https://poser.pugx.org/zoiosilva/oo-sped-nfe/downloads)](https://packagist.org/packages/zoiosilva/oo-sped-nfe)
[![Latest Unstable Version](https://poser.pugx.org/zoiosilva/oo-sped-nfe/v/unstable)](https://packagist.org/packages/zoiosilva/oo-sped-nfe)
[![License](https://poser.pugx.org/zoiosilva/oo-sped-nfe/license)](https://packagist.org/packages/zoiosilva/oo-sped-nfe)
[![Monthly Downloads](https://poser.pugx.org/zoiosilva/oo-sped-nfe/d/monthly)](https://packagist.org/packages/zoiosilva/oo-sped-nfe)
[![Daily Downloads](https://poser.pugx.org/zoiosilva/oo-sped-nfe/d/daily)](https://packagist.org/packages/zoiosilva/oo-sped-nfe)
[![composer.lock](https://poser.pugx.org/zoiosilva/oo-sped-nfe/composerlock)](https://packagist.org/packages/zoiosilva/oo-sped-nfe)

# Object Oriented Sped-NFe
Uma interface orientada a objetos e fortemente tipada para a emissão de notas fiscais eletrôncias, apoiando-se no componente original [SPED-NFe](https://github.com/nfephp-org/sped-nfe).

# 1. Requisitos
Para que este pacote possa funcionar, é necessário ter instalado o php 7.2, inclusive:
* php7.2-cli;
* php7.2-curl;
* php7.2-soap;
* php7.2-mbstring;
* git;
* unzip;
* [Composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx).

# 2. Como usar
Por enquanto, o projeto está suportando o envio síncrono de uma NF-e por vez. Existem 2 classes principais no repositório, que fazem praticamente todo o trabalho. `OO_NFePHP\Builder` e `OO_NFePHP\Sender`:

## 2.1 Classe Builder
Esta classe será reponsável por construir o arquivo xml que será enviado ao servido do estado. Ele também irá cuidar da assinatura digital do documento.

### 2.1.1 Método build
Chamando este método ele irá compilar as informações passadas no construtor da classe, e gerar o arquivo xml, ainda sem assinatura.

### 2.1.2 Método sign
Este método irá fazer a assinatura digital do documento compilado, usando o certificado digital passado por parâmetro.

### 2.2.3 Método getXML
Obtém o XML gerado pela classe. Caso tenha havido algum erro, este método retornará uma string vazia.

### 2.2.4 Método getErrorMessage
Caso tenha havido algum erro, a mensagem de erro pode ser obtida aqui.

## 2.2 Classe Sender
Esta classe é responsável pelo envio do documento xml ao servidor do estado.

### 2.2.1 Método enviarNotaSincrono
Realiza a comunicação com o servidor do estado, já validando e envelopando o xml antes de manda-lo. O envio se dá de forma síncrona, ou seja, o método ficará aguardando a resposta do serivdor remoto. A resposta da comunicação é retornada como _string_.
